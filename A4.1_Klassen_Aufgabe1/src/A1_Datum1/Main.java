package A1_Datum1;

public class Main {

	public static void main(String[] args) {
		
		Datum datum1 = new Datum();
		Datum datum2 = new Datum();
		Datum datum3 = new Datum(13,10,2020);
		
		System.out.println(datum1.toString());
		System.out.println(datum2.toString());
		System.out.println(datum3.toString());
		
		System.out.println("Stimmen Datum 1 und Datum 2 überein?");
		if(datum1.equals(datum2))
			System.out.println("Die beiden Daten stimmen überein.");	
		else
			System.out.println("Die beiden Daten stimmen nicht überein.");
		System.out.println();
		
		datum2.setTag(25);
		datum2.setMonat(11);
		datum2.setJahr(2021);
		
		System.out.println(datum1.toString());
		System.out.println(datum2.toString());
		
		System.out.println("Stimmen Datum 1 und Datum 2 überein?");
		if(datum1.equals(datum2))
			System.out.println("Die beiden Daten stimmen überein.");	
		else
			System.out.println("Die beiden Daten stimmen nicht überein.");
		System.out.println();
		
		System.out.println("Stimmen Datum 2 und Datum 3 überein?");
		if(datum2.equals(datum3))
			System.out.println("Die beiden Daten stimmen überein.");	
		else
			System.out.println("Die beiden Daten stimmen nicht überein.");
		System.out.println();
		
		System.out.println("Welchen Monat besitzt Datum 3? Antwort: " + datum3.getMonat());
	}

}
