package A4_Person;

public class Person {
	private String vorname, nachname;
	private Datum geburtstag;
	
	public Person(String vorname, String nachname, Datum geburtstag) {
		setVorname(vorname);
		setNachname(nachname);
		setGeburtstag(geburtstag);
	}
	
	//SET Befehle
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	public void setGeburtstag(Datum geburtstag) {
		this.geburtstag = geburtstag;
	}
	
	//GET Befehle
	public String getVorname() {
		return this.vorname;
	}
	
	public String getNachname() {
		return this.nachname;
	}
	
	public Datum getGeburtstag() {
		return this.geburtstag;
	}
	
	//Standard Stringausgabe
	@Override
	public String toString() {
		return "{ Personaldaten: \n\t" +
					"Vorname: " + this.vorname + "\n\t" +
					"Nachname: " + this.nachname + "\n\t" +
					"Geburtstag: "+ this.geburtstag + "\n"
					+"}";
	}
}
