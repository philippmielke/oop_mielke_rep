package A4_Person;

public class Main {

	public static void main(String[] args) {
		
		Person p1 = new Person("Philipp", "Mielke", new Datum(28,8,1993));
		
		System.out.println(p1);
		System.out.println();
		System.out.println(p1.getVorname() + " hat am " + 
				p1.getGeburtstag().getTag() + "." +
				p1.getGeburtstag().getMonat() + "." +
				" Geburtstag.");
	}

}
