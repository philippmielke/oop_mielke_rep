package A5_Person_Klassenattribut;

public class Person {
	private static int personenanzahl = 0;
	
	private String vorname, nachname;
	private Datum geburtstag;
	private int matrikelnummer;
	
	public static int getPerosnenanazahl() {
		return personenanzahl;
	}
	
	public Person(String vorname, String nachname, Datum geburtstag, int personalnummer) {
		personenanzahl++;
		setVorname(vorname);
		setNachname(nachname);
		setGeburtstag(geburtstag);
		setMatrikelnummer(personalnummer);
	}
	
	//SET Befehle
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	public void setGeburtstag(Datum geburtstag) {
		this.geburtstag = geburtstag;
	}
	
	public void setMatrikelnummer(int matrikelnummer) {
		this.matrikelnummer = matrikelnummer;
	}
	
	//GET Befehle
	public String getVorname() {
		return this.vorname;
	}
	
	public String getNachname() {
		return this.nachname;
	}
	
	public Datum getGeburtstag() {
		return this.geburtstag;
	}
	
	public int getPersonalnummer() {
		return this.matrikelnummer;
	}
	
	//Standard Stringausgabe
	@Override
	public String toString() {
		return "{ Personaldaten: \n\t" +
					"Vorname: " + this.vorname + "\n\t" +
					"Nachname: " + this.nachname + "\n\t" +
					"Geburtstag: "+ this.geburtstag + "\n\t" +
					"Matrikelnummer: " + this.matrikelnummer + "\n" +
					"}";
	}
}
