package A5_Person_Klassenattribut;

public class Datum {
	
	private int tag, monat, jahr;
	
	public Datum() {
		
		this.tag =  1;
		this.monat = 1;
		this.jahr = 1970;
	}
	
	public Datum(int tag, int monat, int jahr) {
		
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	}
	
	public void setTag(int tag) {
		if (tag > 0 && tag < 31) this.tag = tag;
	}
	
	public int getTag() {
		return this.tag;
	}
	
	public void setMonat(int monat) {
		if (monat > 0 && monat <12) this.monat = monat;
	}
	
	public int getMonat() {
		return this.monat;
	}
	
	public void setJahr(int jahr) {
		this.jahr = jahr;
	}
	
	public int getJahr() {
		return this.jahr;
	}
	
	@Override
	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}
	
	@Override
	public boolean equals(Object obj) {
		String datum1 = toString();
		
		if(obj instanceof Datum) {
			String datum2 = ((Datum) obj).toString();
			return datum1.equals(datum2);
		} else return false;
	}
	
}
