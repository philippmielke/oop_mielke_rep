package A5_Person_Klassenattribut;

public class Main {

	public static void main(String[] args) {
		
		Datum geb1 = new Datum(23,3,1990);
		
		Person p1 = new Person("Philipp", "Mielke", new Datum(28,8,1993), 907061);
		Person p2 = new Person ("Max", "Mustermann", geb1, 807643);
		
		Person[] studenten = new Person[2];
		
		studenten[0] = p1;
		studenten[1] = p2;
		
		for(Person student : studenten) {
			System.out.println(student);
			System.out.println();
			System.out.println(student.getVorname() + " hat am " + 
					student.getGeburtstag().getTag() + "." +
					student.getGeburtstag().getMonat() + "." +
					" Geburtstag.");
			System.out.println();
		}
		
		System.out.println("Es existieren bisher " + Person.getPerosnenanazahl() + " Personen als Objekte.");
	}

}
