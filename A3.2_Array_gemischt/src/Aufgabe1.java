public class Aufgabe1 {

   public static void main(String[] args) {

      double[] temperatur = {  4.0,  3.2,  2.5,  1.9,  1.6,  1.1, 
                               0.3,  0.0,  1.0,  4.9,  8.8, 13.5, 
                              14.1, 15.3, 16.3, 17.1, 17.6, 18.2, 
                              17.2, 15.8, 13.5, 13.2, 13.3, 12.0 };

      System.out.println(berechneMittelwert(temperatur));
      System.out.println(berechneMinimum(temperatur));
      System.out.println(berechneMaximum(temperatur));

   }
   
   public static double berechneMittelwert(double[] temperatur) {
	   
	   double mittelwert = 0;
	   
	   for(int i = 0; i < temperatur.length; i++ ) {
		   
		   mittelwert += temperatur[i];
	   }
	   
	   return mittelwert / temperatur.length;
	   
   }
   
   public static double berechneMinimum(double[] temperatur) {
	   
	   double minimum = temperatur[0];
	   
	   for( int i = 0; i < temperatur.length; i++ ) {
		   
		   if(minimum > temperatur[i]) minimum = temperatur[i];
	   }
	   
	   return minimum;
	   
   }
   
   public static double berechneMaximum(double[] temperatur) {
	   
	   double maximum = temperatur[0];
	   
	   for( int i = 0; i < temperatur.length; i++ ) {
		   
		   if(maximum < temperatur[i]) maximum = temperatur[i];
	   }
	   
	   return maximum;
	   
   }

  
}
