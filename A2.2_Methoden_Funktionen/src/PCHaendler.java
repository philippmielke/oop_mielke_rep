import java.util.Scanner;

public class PCHaendler {

	static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {

		String artikel;
		int anzahl;
		double preis, mwst, nettogesamtpreis, bruttogesamtpreis;

		// Benutzereingaben lesen
		// System.out.println("was moechten Sie bestellen?");
		// String artikel = myScanner.next();

		artikel = liestString("Was moechten Sie bestellen? ");

		// System.out.println("Geben Sie die Anzahl ein:");
		// int anzahl = myScanner.nextInt();

		anzahl = liestInt("Geben Sie die Anzahl ein: ");

		// System.out.println("Geben Sie den Nettopreis ein:");
		// preis = myScanner.nextDouble();

		preis = liestDouble("Geben Sie den Nettopreis ein:");

		// System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		// mwst = myScanner.nextDouble();

		mwst = liestDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");

		// Verarbeiten
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		//System.out.println("\tRechnung");
		//System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		//System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}

	public static String liestString(String text) {
		System.out.println(text);
		return myScanner.next();
	}

	public static int liestInt(String text) {
		System.out.println(text);
		return myScanner.nextInt();
	}

	public static double liestDouble(String text) {
		System.out.println(text);
		return myScanner.nextDouble();
	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}

	public static void rechungausgeben(String artikel, int anzahl, 
			double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
