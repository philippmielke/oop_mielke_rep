package A3_InterfaceIntArray;

public class MyIntArrayTest {

	public static void main(String[] args) {
		
		MyIntArray arr = new MyIntArray();
		
		try{
			arr.set(3, 5);
			arr.set(13, 5);

		} catch (MyIndexException ex) {
			System.out.println(	ex.getMessage() 
								+ " an Index " 
								+ ex.getWrongIndex());
		}
		
		try {
			System.out.println(arr.get(10));
			
		} catch (MyIndexException ex) {
			System.out.println(ex.getMessage()
								+ " an Index "
								+ ex.getWrongIndex());
		} finally {
			arr.increase(5);
		}
		
		try {
			System.out.println(arr.get(10));
			arr.set(13, 5);
			for(int i = 0; i < 15; i++) System.out.println(arr.get(i) + " ");
			
		} catch (MyIndexException ex) {
			System.out.println(ex.getMessage()
								+ " an Index "
								+ ex.getWrongIndex());
		}
	}

}
