package A3_InterfaceIntArray;

public class MyIntArray implements IIntArray{
	
	private int[] array;
	
	public MyIntArray() {
		this.array = new int[10];
	}
	
	public MyIntArray(int laenge) {
		this.array = new int[laenge];
	}
	
	public int get(int index) throws MyIndexException {
		if(index < this.array.length) return this.array[index];
		else throw new MyIndexException(index);
	}
	
	public void set(int index, int value) throws MyIndexException {
		if(index < this.array.length) this.array[index] = value;
		else throw new MyIndexException(index);
	}
	
	public void increase(int n) {
		if(n > 0) {
			int[] newArray = new int[this.array.length + n];
			for(int i = 0; i < this.array.length; i++) newArray[i] = this.array[i];
			this.array = newArray;
		}
	}
}
