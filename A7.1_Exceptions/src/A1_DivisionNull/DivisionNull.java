package A1_DivisionNull;

public class DivisionNull {

	public static void main(String[] args) {
		
		try {
			int ergebnis = getDivision(5, 0);
		} catch (ArithmeticException ex) {
			System.out.println("Division durch Null nicht zulässig.");
		}
		
		try {
			int ergebnis = getDivision(5,0);
		} catch (ArithmeticException ex) {
			System.out.println("Division durch Null unlösbar.");
		}
		
		try {
			int ergebnis = getDivision(5,2);
			System.out.println(ergebnis);
		} catch (ArithmeticException ex) {
			System.out.println("Division durch Null unlösbar.");
		}
		
	}
	
	public static int getDivision(int z1, int z2) {
		return z1 / z2;
	}

}
