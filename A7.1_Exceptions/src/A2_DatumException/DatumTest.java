package A2_DatumException;

public class DatumTest {

	public static void main(String[] args) {
		
		try {
			Datum d1 = new Datum(0,11,2000);
		} catch (DatumException ex) {
			System.out.print(ex.getMessage());
			System.out.println(" " + ex.getFWert());
		}
		
		try {
			Datum d2 = new Datum(13,13,2000);
			System.out.println(d2);
		} catch (DatumException ex) {
						System.out.print(ex.getMessage());
			System.out.println(" " + ex.getFWert());
		}
		
		try {
			Datum d3 = new Datum(10,12,1000);
		} catch (DatumException ex) {
			System.out.println(ex.getMessage()
							+ " "
							+ ex.getFWert());
		}
		
	}

}
