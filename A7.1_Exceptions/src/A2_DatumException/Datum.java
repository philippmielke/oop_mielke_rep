package A2_DatumException;

public class Datum {
	private int tag;
	private int monat;
	private int jahr;
	
	
	public Datum() throws DatumException{
		setTag(1);
		setMonat(1);
		setJahr(1970);
	}

	public Datum(int tag, int monat, int jahr) throws DatumException {
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) throws TagException {
		if(0 < tag &&tag < 32) this.tag = tag;
		else throw new TagException("Der Tag ist ungültig: ", tag);
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) throws MonatException {
		if(0 < monat && monat < 13) this.monat = monat;
		else throw new MonatException("Der Monat ist ungültig: ", monat);
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) throws JahrException {
		if(1900 < jahr && jahr < 2100) this.jahr = jahr;
		else throw new JahrException("Ungültiges Jahr: ", jahr);
	}

	public static int berechneQuartal(Datum d){
		return d.monat/3 +1;
	}
	
	@Override
	public boolean equals(Object obj) {
		Datum d = (Datum) obj;
		if (this.tag == d.tag && this.monat == d.monat && this.jahr == d.jahr)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return this.tag+"."+this.monat+"."+this.jahr;
	}
		

}
