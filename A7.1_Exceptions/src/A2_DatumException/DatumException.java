package A2_DatumException;

public class DatumException extends Exception {

	private int fWert;
	
	public DatumException(String msg, int fWert) {
		super(msg);
		this.fWert = fWert;
	}
	
	public int getFWert() {
		return this.fWert;
	}
}
