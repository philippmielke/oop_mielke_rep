
public class Einmaleins {

	public static void main(String[] args) {
		generiereKopfzeile();
		generiereEinmalEins();		
	}

	public static void formatiereAusgabe(int ergebnis) {	
		if (ergebnis == 0) {
			System.out.printf("%4s", "i/j|");
		} else {
			System.out.printf("%4s", ergebnis);
		}
	}

	public static void generiereKopfzeile() {
		for (int j = 0; j < 11; j++) {
			formatiereAusgabe(j);
		}
		System.out.println();
		for (int j = 0; j < 44; j++) {
			System.out.print("-");
		}
		System.out.println();
	}

	public static void generiereEinmalEins() {	
		for (int i = 1; i < 11; i++) {			
			System.out.printf("%-3s|", i);

			for (int j = 1; j < 11; j++) {
				formatiereAusgabe(i * j);
			}		
			System.out.println();		
		}		
	}	
}
