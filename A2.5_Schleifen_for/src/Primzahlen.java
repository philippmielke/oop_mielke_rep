
public class Primzahlen {

	public static void main(String[] args) {
		
		System.out.printf("Primzahlen:\t%-3s", 1);
		
		for(int i = 2; i < 101; i++) {
			for(int j = 2; j <= i; j++) {
				if(i % j == 0) {
					if(i == j) System.out.printf("%-3s", i);
					else break;
				}
			}
		}
	}
}
