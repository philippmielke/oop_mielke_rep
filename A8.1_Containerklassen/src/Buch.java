
public class Buch implements Comparable<Buch> {

	private String autor;
	private String titel;
	private int isbn;
	
	public Buch() {
		this.autor = "unbekannt";
		this.titel = "unbekannt";
		this.isbn = 0;
	}
	
	public Buch (	String autor,
					String titel,
					int isbn) {
		setAutor(autor);
		setTitel(titel);
		setIsbn(isbn);
	}
	
	public String getAutor() { return this.autor; }
	public void setAutor(String autor) { this.autor = autor; }
	
	public String getTitel() { return this.titel; }
	public void setTitel(String titel) { this.titel = titel; }
	
	public int getIsbn() { return this.isbn; }
	public void setIsbn(int isbn) { this.isbn = isbn; }
	
	@Override
	public String toString() {
		return "[ "
				+ this.titel + "\n"
				+ this.autor + "\n"
				+ this.isbn + " ]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Buch) return ((Buch)  obj).getIsbn() == this.isbn;
		else return false;
	}
	
	@Override
	public int compareTo(Buch b) {
		return getIsbn() - b.getIsbn();
	}
}
