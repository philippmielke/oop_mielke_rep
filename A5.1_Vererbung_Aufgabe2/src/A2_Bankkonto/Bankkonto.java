package A2_Bankkonto;

public class Bankkonto {
	
	/** Attribute **/
	private String kontoinhaber;
	private int kontonummer;
	private double kontostand;
	
	/** Konstruktoren **/
	public Bankkonto() {
		
		setKontoinhaber("unbekannt");
		setKontonummer(0);
		setKontostand(0);
	}
	
	public Bankkonto(String kontoinhaber, int kontonummer, double kontostand) {
		
		setKontoinhaber(kontoinhaber);
		setKontonummer(kontonummer);
		setKontostand(kontostand);
	}
	
	/** SET Befehle **/
	public void setKontoinhaber(String kontoinhaber) {
		
		this.kontoinhaber = kontoinhaber;
	}
	
	public void setKontonummer(int kontonummer) {
		
		this.kontonummer = kontonummer;
	}
	
	public void setKontostand(double kontostand) {
		
		this.kontostand = kontostand;
	}
	
	/** GET Befehle **/
	public String getKontoinhaber() {
		
		return this.kontoinhaber;
	}
	
	public int getKontonummer() {
		
		return this.kontonummer;
	}
	
	public double getKontostand() {
		
		return this.kontostand;
	}
	
	/** Zusatzfunktionen **/ 
	public void einzahlen(double betrag) {
		this.kontostand += betrag;
	}
	
	public void auszahlen(double betrag) {
		
		this.kontostand -= betrag; 
		System.out.printf("Du hast %.2f€ abgehoben.\n", betrag);
	}
	
	@Override
	public String toString() {
		
		return 	"Kontoinhaber " + this.kontoinhaber + "\n" +
				"Kontonummer: " + this.kontonummer + "\n" +
				"Kontostand: " + this.kontostand;
	}
}
