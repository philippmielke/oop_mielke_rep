package A2_Bankkonto;

public class Dispokonto extends Bankkonto {
	
	/** Attribute **/
	private double dispokredit;
	
	/** Konstruktoren **/
	public Dispokonto() {
		
		super();
		setDispokredit(0);
	}
	
	public Dispokonto(String kontoinhaber, int kontonummer, double kontostand, double dispokredit) {
		
		super(kontoinhaber, kontonummer, kontostand);
		setDispokredit(dispokredit);
	}
	
	/** SET Befehle **/
	public void setDispokredit(double dispokredit) {
		
		if(dispokredit > -1) this.dispokredit = dispokredit;
		else this.dispokredit = 0;
	}
	
	/** GET Befehle **/
	public double getDispokredit() {
		
		return this.dispokredit;
	}
	
	/** Zusatzfunktionen **/
	public void auszahlen(double betrag) {
		
		double neuerKontostand = super.getKontostand() - betrag;
		
		if (neuerKontostand >= -1 * this.dispokredit) {
			
			super.setKontostand(neuerKontostand);
			System.out.printf("Du hast %.2f€ abgehoben.\n", betrag);
		} else {
			
			System.out.printf("Du kannst maximal %.2f€ abheben.\n", super.getKontostand() + this.dispokredit);
			super.setKontostand(-1 * this.dispokredit);
		}
	}
	
	@Override
	public String toString() {
		return 	super.toString() + "\n" +
				"Dispokredit: " + this.dispokredit;
	}
}
