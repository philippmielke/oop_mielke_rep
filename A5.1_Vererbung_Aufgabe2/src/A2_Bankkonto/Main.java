package A2_Bankkonto;

public class Main {

	public static void main(String[] args) {
		
		Bankkonto bk1 = new Bankkonto("Max Mustermann", 1234, 10.06);
		Dispokonto dk1 = new Dispokonto("Philipp Mielke", 2808, 3000.76, 100.00);
		
		Dispokonto[] arrayDk = new Dispokonto[4];
		
		for(Dispokonto konto : arrayDk) konto = new Dispokonto(); //wieso bleibt das "null"?
		
		for(int i = 0 ; i < arrayDk.length; i++) arrayDk[i] = new Dispokonto();
		
		arrayDk[0] = new Dispokonto("Kevin Kostnix", 7865, 2500.00, 500);
		//arrayDk[1] = new Bankkonto("Charlotte Wild", 2606, 10000.00);
		arrayDk[1] = new Dispokonto("Charlotte Wild", 2606, 10000.00, 400);
		
		System.out.println();
		bk1.einzahlen(30);
		System.out.println(bk1);
		
		System.out.println();
		bk1.auszahlen(5000);
		System.out.println(bk1);
		
		System.out.println();
		dk1.einzahlen(30);
		System.out.println(dk1);
		
		System.out.println();
		dk1.auszahlen(5000);
		System.out.println(dk1);
		
		for(Dispokonto konto : arrayDk) {
			
			System.out.println();
			System.out.println(konto);
			System.out.println();
		}
	}
}
