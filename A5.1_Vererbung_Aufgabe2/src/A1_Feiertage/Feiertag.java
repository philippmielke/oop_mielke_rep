package A1_Feiertage;

public class Feiertag extends Datum {
	private String feiertag;
	
	public Feiertag() {
		super();
		this.feiertag = "";
	}
	
	public Feiertag (int tag, int monat, int jahr, String feiertag) {
		super(tag, monat, jahr);
		this.feiertag = feiertag;
	}
	
	/** SET Befehle **/
	public void setFeiertag(String feiertag) {
		this.feiertag = feiertag;
	}
	
	/** GET Befehle **/
	public String getFeiertag() {
		return this.feiertag;
	}
	
	@Override
	public String toString() {
		return super.toString() + " ( " + this.feiertag + " )";
	}
	
	@Override
	public boolean equals(Object obj) {
		String istFeiertag1 = this.toString();
		
		if(obj instanceof Feiertag) {
			String istFeiertag2 = ((Feiertag) obj).toString();
			return istFeiertag1.equals(istFeiertag2);
			
			
		}
		return false;
	}
}
