package A1_Feiertage;

public class Main {

	public static void main(String[] args) {
		Feiertag feiertag1 = new Feiertag();
		Feiertag feiertag2 = new Feiertag(28,8,1993,"Geburtstag");
		Feiertag feiertag3 = new Feiertag(1,1,1970,"Neujahr");
		
		feiertag1.setFeiertag("Neujahr");
		
		System.out.println(feiertag1);
		System.out.println(feiertag2);
		System.out.println(feiertag3);
		System.out.println(feiertag1.equals(feiertag3));

	}

}
