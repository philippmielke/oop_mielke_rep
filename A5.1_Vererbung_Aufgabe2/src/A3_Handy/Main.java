package A3_Handy;

public class Main {

	public static void main(String[] args) {

		Handy h = new Handy("Nokia1", "C62");
		System.out.println("h: " + h);
		
		/*
		 * Antwort: h: Handy [firma= Nokia , typ= C62]
		 * Lösung: h: Handy [firma=Nokia1, typ=C62]
		 */
		
		FotoHandy f = new FotoHandy("Nokia1", "TS8", 5000000, "Nokia2");
		System.out.println("f: " + f);
		
		/*
		 * Antwort: f: FotoHandy [firma(super)= Nokia1, typ= TS8, pixel= 5000000, firma= Nokia2]
		 * Lösung: f: FotoHandy [firma(super)=Nokia1, typ=TS8, pixel=5000000, firma=Nokia2]
		 */
		
		System.out.println("f.super: " + ((Handy) f).toString());
		
		/*
		 * Antwort: f.super: FotoHandy [firma(super)= Nokia1, typ= TS8, pixel= 5000000, firma= Nokia2]
		 * Lösung: f: FotoHandy [firma(super)=Nokia1, typ=TS8, pixel=5000000, firma=Nokia2]
		 */
		
		System.out.println("f.klassenName: " + f.klassenName);
		
		/*
		 * Antwort: f.klassenName: FotoHandy
		 * Lösung: f.klassenName: FotoHandy
		 */
		
		System.out.println("f.klassenName: " + ((Handy) f).klassenName);
		
		/*
		 * Antwort: f.klassenName: Handy
		 * Lösung: f.klassenName: Handy
		 */
		
	}

}
