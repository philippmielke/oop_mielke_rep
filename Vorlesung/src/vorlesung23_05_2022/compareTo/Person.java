package vorlesung23_05_2022.compareTo;


public class Person implements Comparable<Person>{
	
	private int personalnummer;
	private String name;
	

	public Person(String name, int personalnummer) {
		setName(name);
		setPersonalnummer(personalnummer);
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getPersonalnummer() {
		return personalnummer;
	}
	public void setPersonalnummer(int personalnummer) {
			this.personalnummer = personalnummer;
	}
	
	public String toString() {   //  [ Name: Max , Personalnummer: 1234 ]
		return "[ Name: " + this.name + " , Personalnummer: " + this.personalnummer + " ]";
	}
	
	@Override
	public int compareTo(Person p) {
		/*
		if(this.personalnummer < p.getPersonalnummer()) return -1;
		else if (this.personalnummer == p.getPersonalnummer()) return 0;
		else return 1;		
		*/
		
		return this.personalnummer - p.getPersonalnummer();
	}
}