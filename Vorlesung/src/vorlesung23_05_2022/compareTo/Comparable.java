package vorlesung23_05_2022.compareTo;

interface Comparable<Datentyp> {
	
	/* 
	 * liefert >0, wenn größer
	 * liefert 0, wenn gleich
	 * liefert <0, wenn kleiner
	*/ 
	
	int compareTo (Datentyp obj);
}
