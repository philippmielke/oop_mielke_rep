package vorlesung23_05_2022.compareTo;

public class Main {

	public static void main(String[] args) {
		
		Person p1 = new Person("Kaja Sumelson", 88);
		Person p2 = new Person("Philipp Mielke", 18);
		
		System.out.println(p1.compareTo(p2));	
	}

}
