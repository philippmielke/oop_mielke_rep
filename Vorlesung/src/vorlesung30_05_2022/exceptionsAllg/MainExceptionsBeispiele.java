package vorlesung30_05_2022.exceptionsAllg;

public class MainExceptionsBeispiele {

	public static void main(String[] args) {
		
		try {
			
			//System.out.println(args[0]);
			int zahl = Integer.parseInt(args[0]); //Parameter: 23
			zahl++;
			System.out.println(zahl);
			
		} catch (ArrayIndexOutOfBoundsException ex) {
			System.out.println("Element nicht vorhanden");
		} catch (NumberFormatException ex) {
			System.out.println("Falsches Format des Parameters.");
			System.out.println(ex);
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		} catch(Exception ex) {
			System.out.println("Fehler!");
		}
	}

}
