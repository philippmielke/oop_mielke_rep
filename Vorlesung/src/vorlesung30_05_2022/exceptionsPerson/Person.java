package vorlesung30_05_2022.exceptionsPerson;


public class Person {
	
	private int personalnummer, alter;
	private String name;
	

	public Person(String name, int personalnummer, int alter) throws AlterNegativExceptions {
		setName(name);
		setAlter(alter);
		setPersonalnummer(personalnummer);
	}
	
	public void setAlter(int alter) throws AlterNegativExceptions {
		if(alter > 0) this.alter = alter;
		else throw new AlterNegativExceptions("Fehler: Ungültiges Alter.", alter);
	}
	public int getAlter() {
		return this.alter;
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getPersonalnummer() {
		return personalnummer;
	}
	public void setPersonalnummer(int personalnummer) {
			this.personalnummer = personalnummer;
	}
	
	public String toString() {   //  [ Name: Max , Personalnummer: 1234 ]
		return "[ Name: " + this.name + " , Personalnummer: " + this.personalnummer + " ]";
	}
	
}