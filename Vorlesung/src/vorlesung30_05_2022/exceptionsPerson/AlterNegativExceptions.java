package vorlesung30_05_2022.exceptionsPerson;

public class AlterNegativExceptions extends Exception {
	
	private int fwert;
	
	public AlterNegativExceptions(String msg, int fwert) {
		super(msg);
		this.fwert = fwert;
	}
	
	public int getFwert() {
		return this.fwert;
	}
}
