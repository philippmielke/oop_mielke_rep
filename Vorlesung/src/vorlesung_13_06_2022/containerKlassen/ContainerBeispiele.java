package vorlesung_13_06_2022.containerKlassen;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.*; //importiert alle Klassen, Packages

public class ContainerBeispiele {
	
	public static void main(String[] args) {
		String str = "str1";
		
		String[] sliste = new String[1];
		sliste[0] = str;
		
		//ArrayList<String> sliste2 = new ArrayList<String>();
		List<String> sliste2 = new ArrayList<String>(10000);
		//LinkedList<String> sliste2 = new LinkedList<String>();
		//<String> sliste2 = new LinkedList<String>();	
		sliste2.add(str);
		sliste2.add("str2");
		sliste2.add("str3");
		sliste2.add(2, "str4");
		
		System.out.println(sliste2);
		
		sliste2.remove("str3");
		sliste2.remove(2);
		
		ArrayList<String> sliste3 = new ArrayList<String>();
		sliste3.add("str10");
		
		sliste2.addAll(sliste3);
		System.out.println(sliste2);
		
		//sliste2.removeAll(sliste3);
		//System.out.println(sliste2);
		
		//sliste2.retainAll(sliste3);
		//System.out.println(sliste2);
		
		for(int i = 0; i < sliste2.size(); i++) System.out.print(sliste2.get(i) + " ");
		System.out.println();
		for(int i = sliste2.size()-1; i > -1; i--) System.out.print(sliste2.get(i) + " ");
		System.out.println();
		for(String s : sliste2) System.out.print(s + " ");
		System.out.println();
		
		Iterator<String> it = sliste2.iterator();
		while(it.hasNext()) {
			System.out.print(it.next() + " ");
		}
		System.out.println();
		
		ListIterator<String> lit = sliste2.listIterator(sliste2.size());
		while(lit.hasPrevious()) {
			System.out.print(lit.previous() + " ");
		}
	}
	
}
