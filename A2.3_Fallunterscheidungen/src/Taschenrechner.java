import java.util.Scanner;

public class Taschenrechner {
	
	static Scanner myScanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		double zahl1, zahl2, ergebnis;
		
		do {
			System.out.println("Bitte gib zwei Zahlen und danach die Rechenoperation ein.");
			
			zahl1 = liesZahlEin();
			zahl2 = liesZahlEin();
			
			switch(liesRechenoperationEin()) {
			case '+':
				ergebnis = zahl1 + zahl2;
				ergebnisAusgabe(ergebnis);
				break;
			case '-':
				ergebnis = zahl1 - zahl2;
				ergebnisAusgabe(ergebnis);
				break;
			case '*':
				ergebnis = zahl1 * zahl2;
				ergebnisAusgabe(ergebnis);
				break;
			case '/':
				ergebnis = zahl1 / zahl2;
				ergebnisAusgabe(ergebnis);
				break;
			default:
				System.out.print("Die Rechenoperation stand nicht zur Debatte! Ciao...");
			}
		}while(abfrageNochEineRechnung());

	}
	
	public static double liesZahlEin() {
		System.out.println("Geb eine Zahl ein: ");
		return myScanner.nextDouble();
	}
	
	public static char liesRechenoperationEin() {
		System.out.println("Welche Rechenoperation ( + ; - ; * ; / ) möchtest du ausführen?");
		return myScanner.next().charAt(0);
	}
	
	public static void ergebnisAusgabe(double ergebnis) {
		System.out.println("Das Ergebnis lautet:\t" + ergebnis);
	}
	
	public static boolean abfrageNochEineRechnung() {
		System.out.println("Möchtest du noch eine Rechnung durchführen? (j/n)");
		
		char abfrage = myScanner.next().charAt(0);
		
		if (abfrage == 'j') return true;
		else return false;
	}

}
