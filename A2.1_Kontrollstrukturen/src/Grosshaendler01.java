
import java.util.Scanner;
public class Grosshaendler01 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner (System.in);
		
		double preis, gesamtpreis, mehrwertsteuer;
		int anzahl;
		
		mehrwertsteuer = 0.18;
		
		System.out.print("Geben Sie den Preis pro Maus ein: ");
		preis = myScanner.nextDouble();
		
		System.out.print("Wie viele Maeuse moechten Sie bestellen? ");
		anzahl = myScanner.nextInt();
		
		gesamtpreis = preis * anzahl;
		mehrwertsteuer *= gesamtpreis;
		gesamtpreis += mehrwertsteuer;
		
		if( anzahl < 10) {
			gesamtpreis += 10;
			System.out.printf("Der Gesamtpreis betraegt %.2f€ und beinhaltet die Mehrwertsteuer in Hoehe von %.2f€.", gesamtpreis, mehrwertsteuer);
			System.out.printf("\nDa Sie weniger als 10 Maeuse bestellt haben, fallen Liefergebuehren in Hoeehe von 10€ an.");
		} else {
			System.out.printf("Der Gesamtpreis betraegt %.2f€ und beinhaltet die Mehrwertsteuer in Hoehe von %.2f€.", gesamtpreis, mehrwertsteuer);
		}
		
	}

}

