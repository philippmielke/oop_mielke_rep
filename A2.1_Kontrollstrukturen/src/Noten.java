
import java.util.Scanner;
public class Noten {

	public static void main(String[] args) {
		
		int note, eingabe;
		Scanner myScanner = new Scanner (System.in);
		
		do {
			System.out.print("Geben Sie ihre Note ein: ");
			note = myScanner.nextInt();

			switch (note) {
				case 1:
					System.out.println("1 = Sehr gut");
					break;
				case 2:
					System.out.println("2 = Gut");
					break;
				case 3:
					System.out.println("3 = Befriedigend");
					break;
				case 4:
					System.out.println("4 = Ausreichend");
					break;
				case 5:
					System.out.println("5 = Mangelhaft");
					break;
				case 6:
					System.out.println("6 = Ungenügend");
					break;
				default:
					System.out.println("Die eingegebene Noten entspricht nicht unserem Schulsystem!");
					break;
			}
			
			System.out.print("Noch eine Note?");
			eingabe = myScanner.nextInt();
			
		} while (eingabe != 0);

	}

}
