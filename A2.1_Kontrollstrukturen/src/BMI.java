
import java.util.Scanner;
public class BMI {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner (System.in);
		
		double koerpergewicht, koerpergroeße;
		int bmi;
		String geschlecht;

		System.out.println("Wir bestimmen nur den Body Maß Index.");
		
		do {
			System.out.print("\nBitte geben Sie ihr Geschlecht (m/w) ein: ");
			
			geschlecht = myScanner.nextLine();
			
			if ( !geschlecht.equals("m") && !geschlecht.equals("w") ) {
				System.out.println("Die Eingabe war leider nicht korrekt. Versuchen Sie es erneut.");
			}
			
		}while( !geschlecht.equals("m") && !geschlecht.equals("w") );
		
		System.out.printf("\nBitte geben Sie ihr Koerpergewicht in kg ein: ");
		
		koerpergewicht = myScanner.nextDouble();
		
		System.out.print("\nBitte geben Sie nun Ihre Koerpergroeße in cm ein: ");
		
		koerpergroeße = myScanner.nextDouble() / 100;
		
		bmi = (int)(koerpergewicht / koerpergroeße / koerpergroeße);
		
		switch(geschlecht) {
			case "m": 	if(bmi < 20) {  
							System.out.println("Sie haben Untergewicht!");
						} else if(19 < bmi && bmi < 26){
							System.out.println("Alles in Ordnung.");
						} else {
							System.out.println("Sie leiden an Uebergewicht!");
						}
						break;
			case "w":	if(bmi < 19) {  
							System.out.println("Sie haben Untergewicht!");
						} else if(18 < bmi && bmi < 25){
							System.out.println("Alles in Ordnung.");
						} else {
							System.out.println("Sie leiden an Uebergewicht!");
						}
						break;
		}
	}
}
