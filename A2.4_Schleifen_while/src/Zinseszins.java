import java.util.Scanner;

public class Zinseszins {
	
	static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {
		int laufzeit, i;
		double kapital, zinssatz;
		
		i = 0;
		laufzeit = liesInt("Laufzeit (in ganzen Jahren) des Sparvertrags: ");
		kapital = liesDouble("Wie viel Kapital (in Euro) möchten Sie anlegen: ");
		zinssatz = liesDouble("Zinssatz (in Prozent): ") / 100;
		
		ausgabeKapital("Eingezahltes Kapital:", kapital);
		
		while(i < laufzeit) {
			kapital = berechneNeuesKapital(kapital, zinssatz);
			i++;	
		}
		
		ausgabeKapital("Ausgezahltes Kapital:", kapital);

	}
	
	public static int liesInt(String text) {
		System.out.println(text);
		return myScanner.nextInt();
	}
	
	public static double liesDouble(String text) {
		System.out.println(text);
		return myScanner.nextDouble();
	}
	
	public static double berechneNeuesKapital(double kapital, double zinssatz) {
		return kapital * (1 + zinssatz);
	}
	
	public static void ausgabeKapital(String text, double kapital) {
		System.out.printf("%s\t%.2f\n",text, kapital);
	}

}
