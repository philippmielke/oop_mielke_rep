package ExceptionTime;

public interface ITime {
	/*
	 * Rückhabe: int hours
	 */
	public int getHours();
	
	/*
	 * Rückgabe: int minutes
	 */
	public int getMinute();
	
	/*
	 * Rückgabe: int seconds
	 */
	
	/*
	 * 	Ausgabeformat hh:mm:ss
	*/
	@Override
	public String toString() ;
}
