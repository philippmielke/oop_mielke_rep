package ExceptionTime;

public class Time implements Comparable<Time>{
	private int hours;
	private int minutes;
	private int seconds;
	
	public static void main(String[] args) {
		Time t1 = new Time();
		
		try {
			Time t2 = new Time(5,61,7);
		} catch (TimeException err) {
			System.out.println(err.getMessage());
		} finally {
			System.out.println("Weiter gehts...");
		}
		
		try {
			Time t3 = new Time(5,59,7);
			System.out.println(t1.compareTo(t3));
		} catch (TimeException err) {
			System.out.println(err.getMessage());
		}
		
	}

	public Time(int h, int m, int s) throws TimeException {
		if(-1<h && h < 25) hours = h;
		else throw new TimeException();
		
		if(-1<m && m<60) minutes = m;
		else throw new TimeException();
		
		if(-1<s && s<60) seconds = s;
		else throw new TimeException();
	}

	public Time() {
		hours = 0;
		minutes = 0;
		seconds = 0;
	}

	public int getHours() {
		return hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public int getSeconds() { 
		return seconds;
	}
	
	@Override
	public String toString() {
		return this.hours 
				+ ":" + this.minutes
				+ ":" + this.seconds;
	}
	
	@Override
	public int compareTo(Time t) {
		return toString().compareTo(t.toString());
	}
}
