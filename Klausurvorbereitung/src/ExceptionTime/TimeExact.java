package ExceptionTime;

public class TimeExact extends Time {
	private int zsecond, hsecond;
	
	public TimeExact() {
		super();
		
		this.zsecond = 0;
		this.hsecond = 0;
	}
	
	public TimeExact(	int h,
						int m,
						int s,
						int zs,
						int hs) throws TimeException{
		super(h,m,s);
		
		if(-1<zs && zs < 10) this.zsecond = zs;
		else throw new TimeException();
		
		if(-1<hs && hs<10) this.hsecond = hs;
		else throw new TimeException();
	}
	
	@Override
	public String toString() {
		return this.getHours() +":"
				+ this.getMinutes() + ":"
				+ this.getSeconds() + ":"
				+ this.zsecond +":"
				+ this.hsecond;
	}
	
	public static void main(String[] args) {
		TimeExact t1 = new TimeExact();
		System.out.println(t1);
		
		try {TimeExact t2 = new TimeExact(5,2,3,10,9);}
		catch (TimeException err) {System.out.println(err.getMessage());}
		
		try {TimeExact t3 = new TimeExact(5,2,3,7,9); System.out.println(t3);}
		catch (TimeException err) {System.out.println(err.getMessage());}
		finally {System.out.println("Ende.");}
	}
}
