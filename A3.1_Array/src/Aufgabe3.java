import java.util.Scanner;

public class Aufgabe3 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		char[] zeichen = new char[5];
		
		for(int i = 0 ; i < zeichen.length ; i++) {
			
			System.out.print("Zeichen: ");
			zeichen[i] = myScanner.next().charAt(0);
		}
		
		for (int i = zeichen.length - 1 ; -1 < i ; i--) {
			
			System.out.printf("%2c", zeichen[i]);
		}

	}

}
