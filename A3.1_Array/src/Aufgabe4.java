
public class Aufgabe4 {

	public static void main(String[] args) {
		
		int[] arrayLottozahlen = {3, 7, 12, 18, 37, 42};
		boolean zwoelf, dreizehn;
		
		zwoelf = false;
		dreizehn = false;
		
		/*
		for( int i = 0; i < arrayLottozahlen.length; i++ ) {
			
			if(arrayLottozahlen[i] == 12) zwoelf = true;
						
			if(arrayLottozahlen[i] == 13) dreizehn = true;
			
		}
		*/
		
		for(int i : arrayLottozahlen) {
			
			if( i == 12) zwoelf = true;
			if( i == 13) dreizehn = true;
		}
		
		if(zwoelf) System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
		else System.out.println("Die Zahl 12 ist in der Ziehung nicht enthalten.");
		
		if(dreizehn) System.out.print("Die Zahl 13 ist in der Ziehung enthalten.");
		else System.out.print("Die Zahl 13 ist in der Ziehung nicht enthalten.");

	}

}
