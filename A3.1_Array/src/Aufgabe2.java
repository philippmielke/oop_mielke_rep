
public class Aufgabe2 {

	public static void main(String[] args) {
		
		int j = 0;
		int[] ungradeZahlen = new int[10];
		
		for(int i = 0; i < 20; i++) {
			
			if(i  % 2 != 0 && j < ungradeZahlen.length) {
				ungradeZahlen[j] = i;
				j++;
			}
		}
		
		for(int i = 0 ; i < ungradeZahlen.length ; i++) {
			
			System.out.printf("%3d", ungradeZahlen[i]);
		}

	}

}
