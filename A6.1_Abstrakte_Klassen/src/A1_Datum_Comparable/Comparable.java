package A1_Datum_Comparable;

public interface Comparable <Type> {
	int compareTo (Type obj);
}
