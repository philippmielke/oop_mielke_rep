package A1_Datum_Comparable;

public class Main {

	public static void main(String[] args) {
		
		Datum d1 = new Datum(28,8,1993);
		Datum d2 = new Datum (23,5,2022);
		Datum[] arrDaten = new Datum [2];
		
		for(int i = 0; i < arrDaten.length; i++) arrDaten[i] = new Datum();
		
		System.out.println(d1.compareTo(d2));
		System.out.println(arrDaten[0].compareTo(arrDaten[1]));
		System.out.println(d2.compareTo(d1));

	}

}
