package A3_Stack;

public class MyStackTest {

	public static void main(String[] args) {
		
		MyStack stack = new MyStack();
		
		for(int i = 0; i < 2; i++) {
			for(int j = 0; j < 13; j++) stack.push(j);
			
			System.out.println(stack);
			
			try {
				for(int j = 0; j < 100; j++) stack.pop();
			} catch (MyStackException err) {
				System.out.println( err.getMessage());
			} finally {
				System.out.println(stack);
				System.out.println("Ende!");
			}
			
			System.out.println();
		}
		
		stack.push(30);
		System.out.println(stack);

	}

}
