package A3_Stack;

public class MyStack implements IMyStack {
	
	private int[] stack;
	private int index;
	
	public MyStack() {
		this.stack = new int[10];
		this.index = 0;
	}
	
	public MyStack(int length) {
		this.stack = new int[length];
		this.index = 0;
	}
	
	@Override
	public void push(int value){
		
		if(this.index < this.stack.length) {
			
			this.stack[this.index] = value;
			this.index++;
			
		} else {
			
			int[] newArr = new int[this.index + 1];
			for(int i = 0; i < this.stack.length; i++) newArr[i] = this.stack[i];
			newArr[this.index] = value;
			//for(int i = 0; i < this.stack.length; i++) newArr[i] = this.stack[i];
			this.stack = newArr;
			this.index++;
			System.out.println("Der Stack wurde um 1 Element vergroeßert und beinhaltet nun "
					+ this.index 
					+ " Elemente.");
			
		}
				
	}
	
	@Override
	public int pop() throws MyStackException {
		
		if(this.index != 0) {
			
			this.index--;
			return this.stack[this.index];		
			
		} else {
			
			throw new MyStackException("Der Stack ist leer. Der Index betraegt: "
										+ this.index);
		}
		
	}
	
	@Override
	public String toString() {
		
		String stackInput = "[ ";
		for(int i = 0; i < this.index; i++) stackInput += this.stack[i] + " ";
		stackInput += "]";
		return stackInput;
		
	}

}
