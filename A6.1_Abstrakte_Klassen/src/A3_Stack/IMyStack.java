package A3_Stack;

public interface IMyStack {
	
	public void push(int value);
	
	public int  pop() throws MyStackException;
	
}
